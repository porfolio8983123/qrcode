import './App.css';
import QRCode from 'react-qr-code';
import { useState, useEffect } from 'react';

function App() {
  const [url, setUrl] = useState([]);

  const getUrl = async () => {
    try {
      await fetch('https://loriserver.onrender.com/api/v1/qr')
        .then(response => response.json())
        .then(data => setUrl(data.data));
    } catch (error) {
      console.log(error.message);
    }
  }

  useEffect(() => {
    getUrl();
  }, []);

  console.log(url);

  return (
    <div className="App" style={{ flexDirection: 'column' }}>
      {url.map((element) => (
        <div style={{ marginBottom: 30 }} key={element._id}>
          {element.count > 0 ? (
            <>
              <QRCode value={`https://loriserver.onrender.com/api/v1/qr/${element._id}`} />
              <p style={{ fontSize: 30 }}>{element.count}</p>
            </>
          ) : (
            <p style={{ fontSize: 30 }}>Scan Term Finished</p>
          )}
        </div>
      ))}
    </div>
  );
}

export default App;
